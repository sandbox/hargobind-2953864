/**
 * @file
 */

(function($) {
  Drupal.behaviors.FullcalendarCustom = {
    attach: function(context, settings) {
      settings.fullcalendar_blocks = settings.fullcalendar_blocks || Drupal.settings.fullcalendar_blocks;

      // Load the calendars.
      $.each(settings.fullcalendar_blocks, function(idx, calendar_settings) {
        //calendar_settings.params.viewElementSelector = calendar_settings.selector;
        if (typeof calendar_settings.disableEventClick != 'undefined' && calendar_settings.disableEventClick) {
          calendar_settings.params.eventClick = function() { return false; }
        }

        // Modifications once the event is rendered.
        calendar_settings.params.eventRender = function(event, element, view) {
          // If it's an all-day event, remove the time and marker.
          var is_all_day = ($('.fc-list-item-time', element).text() == 'all-day') || calendar_settings.hideTimes;
          if (is_all_day) {
            $('.fc-list-item-time', element).remove();
            $('.fc-list-item-marker', element).remove();
          }

          // Compact display.
          if (view.options.customDisplayStyle == 'compact') {
            // Replace a row's cells with simplified text.
            if (calendar_settings.disableEventClick) {
              var $title = $('.fc-list-item-title', element).text();
            }
            else {
              var $title = $('.fc-list-item-title', element).html();
            }
            var $time = $('.fc-list-item-time', element).text();
            var $row = $('<td>')
              .append($('<div>').append($('<em>').append($time)))
              .append($('<div>').append($title));
            $(element).empty().append($row);
          }
          // Normal display.
          else {
            // Fix the columns on an all-day event.
            if (is_all_day) {
              $('.fc-list-item-title', element).attr('colspan', 3);
            }
          }
          
          // Remove the class that adds the URL cursor.
          if (calendar_settings.disableEventClick) {
            $(element).removeClass('fc-has-url');
          }
          else {
            $('a[href]', element).attr('target', '_blank');
          }
        }
        
        // Scroll to the current day.
        calendar_settings.params.eventAfterAllRender = function(view) {
          var today = new Date(new Date((new Date()).toDateString()).toISOString().replace(/T\d+/, "T00"));
          var hasPastRows = false;
          var $todayRow = [];
          $('.fc-list-heading', view.el).each(function(i, row) {
            // No need for colSpans on compact displays.
            if (view.options.customDisplayStyle == 'compact') {
              $('td', row).attr('colspan', null);
            }
            
            var row_date = new Date($(row).data('date'));
            if (row_date >= today) {
              $todayRow = $(row);
              return false;
            }
            else {
              hasPastRows = true;
            }
          });
          if (hasPastRows && $todayRow.length) {
            var $scroller = $('.fc-scroller');
            $scroller.scrollTop($todayRow.position().top + $scroller.scrollTop());
          }
        };

//        // Determine the display style based on the region that the calendar is in.
//        calendar_settings.params.customDisplayStyle = 'normal';
//        if ($(calendar_settings.params.viewParentSelector).closest('.region').hasClass('sidebar')) {
//          calendar_settings.params.customDisplayStyle = 'compact';
//          $.extend(true, calendar_settings.params, calendar_settings.compactDisplayParams);
//        }
        // Set the parameters for the custom display.
        if (calendar_settings.params.customDisplayStyle == 'compact') {
          $.extend(true, calendar_settings.params, calendar_settings.compactDisplayParams);
        }

        // Render the calendar.
        $(calendar_settings.params.viewParentSelector).once('fullcalendar-blocks').fullCalendar(calendar_settings.params);
      });
    }
  };
})(jQuery);
