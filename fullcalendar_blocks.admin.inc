<?php

/**
 * @file
 * Admin pages.
 */


/**
 * General settings page.
 */
function fullcalendar_blocks_admin_page() {

  $blocks = variable_get('fullcalendar_blocks_blocks_settings', array());
  $has_google_calendar = FALSE;

  // Custom blocks list.
  $rows = array();
  foreach ($blocks as $block) {
    $delta = $block['delta'];
    $rows[] = array(
      check_plain($block['admin_title']),
      check_plain($block['block_title']),
      'edit' => l(t('Edit'), "admin/config/user-interface/fullcalendar-blocks/edit/$delta"),
      'delete' => l(t('Delete'), "admin/config/user-interface/fullcalendar-blocks/delete/$delta"),
      'block' => l(t('Configure block'), "admin/structure/block/manage/fullcalendar_blocks/$delta/configure"),
    );

    // Check to see if there is a google calendar source.
    $sources = explode("\n", trim($block['events_sources']));
    foreach ($sources as $source) {
      if (valid_email_address($source)) {
        $has_google_calendar = TRUE;
      }
    }
  }

  if ($has_google_calendar && empty(variable_get('fullcalendar_blocks_google_calendar_api_key', ''))) {
    drupal_set_message(t('You need to add the Google Calendar API Key on the settings page for blocks to work.'), 'warning');
  }

  $header = array(t('Admin title'), t('Block title'), array('data' => t('Operation'), 'attributes' => array('colspan' => 3)));
  $form['blocks_list'] = array(
    '#theme' => 'table',
    '#title' => t('Blocks list'),
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No blocks have been configured.'),
  );
  $form['add_block'] = array(
    '#markup' => '<p>' . l(t('Add a new block'), 'admin/config/user-interface/fullcalendar-blocks/add') . '</p>',
  );

  return $form;
}

/**
 * General settings page.
 */
function fullcalendar_blocks_admin_settings_form($form, &$form_state) {
  $form['fullcalendar_blocks_google_calendar_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Calendar API Key'),
    '#description' => t('Enter the API key. See <a href="@url" target="_blank">this page</a> for instructions on generating an API key.',
      array('@url' => 'https://fullcalendar.io/docs/google-calendar')
    ),
    '#default_value' => variable_get('fullcalendar_blocks_google_calendar_api_key', ''),
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['advanced_settings'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advanced_settings']['fullcalendar_blocks_global_json_settings'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom settings in JSON format'),
    '#description' => t('Use this box to define <a href="@url" target="_blank">FullCalendar settings</a> to change the behavior/rendering of the calendar. This applies to all calendar blocks.', array('@url' => 'https://fullcalendar.io/docs')),
    '#default_value' => $settings['fullcalendar_blocks_global_json_settings'],
    '#rows' => 4,
    '#cols' => 60,
  );

  return system_settings_form($form);
}

/**
 * Validation handler.
 *
 * @see fullcalendar_blocks_admin_settings_form().
 */
function fullcalendar_blocks_admin_settings_form_validate($form, &$form_state) {
  $custom_settings = $form_state['values']['fullcalendar_blocks_global_json_settings'];
  if (!empty($custom_settings) && !drupal_json_decode($custom_settings)) {
    form_set_error('fullcalendar_blocks_global_json_settings', t('There is a syntax error in the %name field.', array('%name' => t('Custom settings'))));
  }
}

/**
 * Settings for a calendar block.
 */
function fullcalendar_blocks_block_edit_form($form, &$form_state, $delta = '') {
  $settings = array();
  if (!empty($delta)) {
    $delta = check_plain($delta);
    $settings = fullcalendar_blocks_block_load($delta);
  }

  if (!empty($delta) && !$settings) {
    drupal_set_message(t('Unable to find the settings for block %delta.', array('%delta' => $delta)), 'error');
    drupal_goto('admin/config/user-interface/fullcalendar-blocks');
  }
  else {
    // Load default settings.
    $settings += array(
      'display_type' => 'normal',
      'default_view' => 'listWeek',
      'widget_height' => 270,
      'event_text_color' => 'black',
    );
  }

  $form['origin_delta'] = array(
    '#type' => 'value',
    '#value' => $delta,
  );

  $form['admin_title'] = array(
    '#title' => t('Administrative title'),
    '#type' => 'textfield',
    '#default_value' => $settings['admin_title'],
    '#description' => t('The human-readable name of this block. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['delta'] = array(
    '#type' => 'machine_name',
    '#default_value' => str_replace('-', '_', $delta),
    '#maxlength' => 32,
    '#disabled' => !empty($delta),
    '#machine_name' => array(
      'exists' => 'fullcalendar_blocks_block_load',
      'source' => array('admin_title'),
    ),
  );

  $form['block_title'] = array(
    '#title' => t('Block title'),
    '#type' => 'textfield',
    '#default_value' => $settings['block_title'],
    '#description' => t('The title of the block that will display to visitors.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['events_sources'] = array(
    '#type' => 'textarea',
    '#title' => t('Events sources'),
    '#description' => t('Where the events are fetched from. Multiple sources can be shown. Enter one source per line. This can either be a <a href="@url-google" target="_blank">Google Calendar ID</a> or a <a href="@url-feed" target="_blank">URL to a JSON feed</a>.',
      array('@url-feed' => 'https://fullcalendar.io/docs/events-json-feed', '@url-google' => 'https://fullcalendar.io/docs/google-calendar')),
    '#default_value' => $settings['events_sources'],
    '#rows' => 4,
    '#cols' => 60,
  );

  $form['display_type'] = array(
    '#type' => 'radios',
    '#title' => t('Display type'),
    '#description' => t('The "normal" display will show time, a marker, and the event name in the same row. The "small" display will show the time above the event name.'),
    '#default_value' => $settings['display_type'],
    '#options' => array(
      'normal' => t('Normal'),
      'compact' => t('Compact'),
    ),
  );

  $form['default_view'] = array(
    '#type' => 'select',
    '#title' => t('Default view'),
    '#description' => t('Pick how this calendar should display. See the "Views" section in the <a href="@url" target="_blank">documentation</a>.',
      array('@url' => 'https://fullcalendar.io/docs')
    ),
    '#default_value' => $settings['default_view'],
    '#options' => fullcalendar_blocks_get_views_list(),
  );

  $form['widget_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Widget height'),
    '#description' => t('Set the height in pixels. Use only digits.'),
    '#default_value' => $settings['widget_height'],
    '#size' => 5,
    '#maxlength' => 255,
  );

  $form['event_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Event text color'),
    '#description' => t('Default text color of events.'),
    '#default_value' => $settings['event_text_color'],
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['hide_times'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide the times on an event'),
    '#description' => t('Hide the times for an event (similar to an All-Day display).'),
    '#default_value' => $settings['hide_times'],
    '#size' => 60,
    '#maxlength' => 255,
  );

  $form['disable_event_click'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable event clicking'),
    '#description' => t('Prevents the default behavior of taking the user to the calendar.'),
    '#default_value' => $settings['disable_event_click'],
    '#size' => 60,
    '#maxlength' => 255,
  );

  $content_above = $settings['content_above'];
  $form['content_above'] = array(
    '#type' => 'text_format',
    '#title' => t('Content above the calendar'),
    '#description' => t('Enter content which will appear above the calendar.'),
    '#default_value' => $content_above['value'],
    '#format' => $content_above['format'],
  );
  $content_below = $settings['content_below'];
  $form['content_below'] = array(
    '#type' => 'text_format',
    '#title' => t('Content below the calendar'),
    '#description' => t('Enter content which will appear below the calendar.'),
    '#default_value' => $content_below['value'],
    '#format' => $content_below['format'],
  );

  $form['advanced_settings'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced_settings']['custom_settings'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom settings in JSON format'),
    '#description' => t('Use this box to define <a href="@url" target="_blank">FullCalendar settings</a> to change the behavior/rendering of the calendar.', array('@url' => 'https://fullcalendar.io/docs')),
    '#default_value' => $settings['custom_settings'],
    '#rows' => 4,
    '#cols' => 60,
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validation handler.
 *
 * @see fullcalendar_blocks_block_edit_form().
 */
function fullcalendar_blocks_block_edit_form_validate($form, &$form_state) {
  $machine_name = $form_state['values']['delta'];
  if (!preg_match('#^[a-z0-9_]+$#', $machine_name)) {
    form_set_error('machine_name', t('The machine name has invalid characters.'));
  }
  $custom_settings = $form_state['values']['custom_settings'];
  if (!empty($custom_settings) && !drupal_json_decode($custom_settings)) {
    form_set_error('custom_settings', t('There is a syntax error in the %name field.', array('%name' => t('Custom settings'))));
  }
  $form_state['values']['widget_height'] = (int)$form_state['values']['widget_height'];
}

/**
 * Submit handler.
 *
 * @see fullcalendar_blocks_block_edit_form().
 */
function fullcalendar_blocks_block_edit_form_submit($form, &$form_state) {
  $settings = $form_state['values'];
  unset($settings['origin_delta']);
  unset($settings['submit']);
  unset($settings['form_build_id']);
  unset($settings['form_token']);
  unset($settings['form_id']);
  unset($settings['op']);

  $delta = str_replace('_', '-', $settings['delta']);
  $settings['delta'] = $delta;

  $blocks = variable_get('fullcalendar_blocks_blocks_settings', array());
  unset($blocks[$form_state['values']['origin_delta']]);
  $blocks[$delta] = $settings;
  variable_set('fullcalendar_blocks_blocks_settings', $blocks);

  drupal_set_message(t('Block settings saved.'));
  drupal_goto('admin/config/user-interface/fullcalendar-blocks');
}

/**
 * Delete block form.
 */
function fullcalendar_blocks_block_confirm_delete($form, &$form_state, $delta) {
  $delta = check_plain($delta);
  $block = fullcalendar_blocks_block_load($delta);
  $form['delta'] = array('#type' => 'value', '#value' => $delta);
  return confirm_form(
    $form,
    t('Are you sure you want to delete the block %title?', array('%title' => $block['admin_title'])),
    'admin/config/user-interface/fullcalendar-blocks',
    t('This action cannot be undone.'),
    t('Delete'), t('Cancel')
  );
}

/**
 * Submit handler.
 *
 * @see fullcalendar_blocks_block_confirm_delete().
 */
function fullcalendar_blocks_block_confirm_delete_submit($form, &$form_state) {
  db_delete('block')
    ->condition('module', 'fullcalendar_blocks')
    ->condition('delta', $form_state['values']['delta'])
    ->execute();

  $blocks = variable_get('fullcalendar_blocks_blocks_settings', array());
  unset($blocks[$form_state['values']['delta']]);
  variable_set('fullcalendar_blocks_blocks_settings', $blocks);

  drupal_set_message(t('Block has been deleted.'));
  $form_state['redirect'] = 'admin/config/user-interface/fullcalendar-blocks';
}
