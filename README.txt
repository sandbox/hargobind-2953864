
This module extends the FullCalendar module, which is based on the
FullCalendar library of the same name: https://fullcalendar.io/

This module adds blocks with configuration options to render a calendar. Some
display improvements have been added to make the calendar display better in
the sidebar of a page. You can configure Event Sources from a Google Calendar
or a JSON feed URL (or both).
